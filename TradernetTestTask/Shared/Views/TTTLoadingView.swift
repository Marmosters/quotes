//
//  TTTLoadingView.swift
//  TradernetTestTask
//
//  Created by Vladimir on 20.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import UIKit

class TTTLoadingView: UIView {
    private var spinner: UIActivityIndicatorView!

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        spinner = UIActivityIndicatorView(style: .large)
        spinner.color = .white
        spinner.translatesAutoresizingMaskIntoConstraints = false
        addSubview(spinner)
        spinner.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    func startSpinner() {
        spinner.startAnimating()
    }
    
    func stopSpinner() {
        spinner.stopAnimating()
        removeFromSuperview()
    }
    
    func raiseError() {
        
    }
}
