//
//  TTTTicker.swift
//  TradernetTestTask
//
//  Created by Vladimir on 21.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import Foundation

struct TTTTicker: Codable {
    let id: Int
    let name: String
    let defaultTicker: String
    let ntTicker: String
    let firstDate: String
    let currency: String
    let minStep: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "short_name"
        case defaultTicker = "default_ticker"
        case ntTicker = "nt_ticker"
        case firstDate
        case currency
        case minStep = "min_step"
    }
}
