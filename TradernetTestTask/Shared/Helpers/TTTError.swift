//
//  Error + extensions.swift
//  TradernetTestTask
//
//  Created by Vladimir on 20.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import Foundation

enum TTTError: Error {
    case badURL
}
