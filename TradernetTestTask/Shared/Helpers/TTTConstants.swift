//
//  TTTConstants.swift
//  TradernetTestTask
//
//  Created by Vladimir on 20.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import UIKit

struct TTTNibs {
    static let qouteCellIdentifer = "qouteCell"
}

struct TTTColors {
    static let normalText = UIColor.black
    static let higlightedText = UIColor.white
    static let positiveTrandColor = UIColor(red: 0.41, green: 0.73, blue: 0.33, alpha: 1.00)
    static let negativeTrandColor = UIColor(red: 0.76, green: 0.12, blue: 0.25, alpha: 1.00)
}
