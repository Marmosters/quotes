//
//  Double + fortmattedString.swift
//  TradernetTestTask
//
//  Created by Vladimir on 21.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import Foundation

extension Double {
    func formattedString(minFraction: Int, maxFraction: Int, withSign: Bool ) -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = minFraction
        formatter.maximumFractionDigits = maxFraction
        let formatedString =  String(formatter.string(from: number) ?? "")
        guard withSign else {return formatedString}
        return self <= 0 ? formatedString : "+" + formatedString
    }
}
