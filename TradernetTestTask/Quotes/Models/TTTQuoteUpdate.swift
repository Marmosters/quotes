//
//  TTTQuoteUpdate.swift
//  TradernetTestTask
//
//  Created by Vladimir on 21.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import Foundation

enum TTTQuoteTrend {
    case up
    case down
    case none
}

struct TTTQuoteUpdate {
    let ticker: String
    var diffPercent: Double!
    var lastStockExchange: String!
    var name: String!
    var lastPrice: Double!
    var diffPoints: Double!
    var trend: TTTQuoteTrend = .none
    
    init(withDict dict: [String: Any]) {
        ticker = dict["c"] as? String ?? ""
        diffPercent = dict["pcp"] as? Double
        lastStockExchange = dict["ltr"] as? String
        name = dict["name"] as? String
        lastPrice = dict["ltp"] as? Double
        diffPoints = dict["chg"] as? Double
    }
}
