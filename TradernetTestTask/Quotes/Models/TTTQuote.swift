//
//  TTTQuote.swift
//  TradernetTestTask
//
//  Created by Vladimir on 21.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import UIKit

struct TTTQuote {
    let ticker: TTTTicker
    var logo: UIImage?
    var quote: TTTQuoteUpdate?
    var isUpdated = false
}
