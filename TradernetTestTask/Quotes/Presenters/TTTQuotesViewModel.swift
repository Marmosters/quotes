//
//  TTTQuotesViewModel.swift
//  TradernetTestTask
//
//  Created by Vladimir on 20.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay


enum TTTFetchDataState {
    case loadingDidStart
    case loadingDidEnd
}

class TTTQuotesViewModel {
    
    private(set) var quotes = [BehaviorRelay<TTTQuote>]()
    private(set) var tableModel = PublishSubject<[BehaviorRelay<TTTQuote>]>()
    private(set) var loadingState = PublishSubject<TTTFetchDataState>()
    private let tickers = ["RSTI", "GAZP", "MRKZ", "RUAL", "HYDR", "MRKS", "SBER", "FEES", "TGKA", "VTBR", "ANH.US", "VICL.US", "BURG.US", "NBL.US", "YETI.US", "WSFS.US", "NIO.US", "DXC.US", "MIC.US", "HSBC.US", "EXPN.EU", "GSK.EU", "SHP.EU", "MAN.EU", "DB1.EU", "MUV2.EU", "TATE.EU", "KGF.EU", "MGGT.EU", "SGGD.EU"]
    
    private let quotesServerUrl = "https://ws3.tradernet.ru"
    
    private let quotesServive = TTTQuotesNetworkService()
    
    func subscribeToQuotes() {
        quotesServive.delegate = self
        quotesServive.subscribeToQuotes(tickers)
    }
    
    func getTickers() {
        loadingState.onNext(.loadingDidStart)
        var tickersData = [TTTQuote]()
        DispatchQueue.global().async { [weak self] in
            guard let tickers = self?.tickers else {return}
            let group = DispatchGroup()
            for ticker in tickers {
                group.enter()
                self?.quotesServive.getTickerInfo(ticker: ticker) { data, error in
                    if let data = data {
                        tickersData.append(data)
                    }
                    group.leave()
                }
            }
            group.wait()

            DispatchQueue.main.async {
                self?.loadingState.onNext(.loadingDidEnd)
                guard let self = self else {return}
                tickersData = self.tickers.compactMap{ ticker in
                    tickersData.filter{ $0.ticker.ntTicker == ticker }.first
                }
                self.quotes = tickersData.compactMap({BehaviorRelay(value: $0)})
                self.tableModel.onNext(self.quotes)
                self.subscribeToQuotes()
            }
        }
    }
}

extension TTTQuotesViewModel: TTTQuotesDataDelegate {
    var quotesServer: String {
        return quotesServerUrl
    }
    
    var tickersToWatch: [String] {
        return tickers
    }
    
    func quoteDidUpdated(_ data: [TTTQuoteUpdate]) {
        for quote in data {
            guard let index = quotes.firstIndex(where: {$0.value.ticker.ntTicker == quote.ticker}) else {continue}
            var qurValue = quotes[index].value
            guard var qurQuote = qurValue.quote else {
                qurValue.quote = quote
                quotes[index].accept(qurValue)
                continue
            }

            if let diffPercent = quote.diffPercent {
                qurQuote.diffPercent = diffPercent
            }
            
            if let lastStockExchange = quote.lastStockExchange {
                qurQuote.lastStockExchange = lastStockExchange
            }
            
            if let lastPrice = quote.lastPrice {
                let prevPrice =  quotes[index].value.quote?.lastPrice
                qurQuote.lastPrice = lastPrice
                if lastPrice != prevPrice ?? lastPrice {
                    qurQuote.trend = lastPrice > prevPrice! ? .up : .down
                }
            }
            
            if let difPoints = quote.diffPoints {
                qurQuote.diffPoints = difPoints
            }
            qurValue.quote = qurQuote
            quotes[index].accept(qurValue)
        }
    }
    
    func quotesUpdateServiceDidConnected() {
        
    }
    
    func quotesUpdateServiceDidDisconected() {
        
    }
}
