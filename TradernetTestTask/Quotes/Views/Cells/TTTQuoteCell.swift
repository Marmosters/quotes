//
//  TTTQuoteCell.swift
//  TradernetTestTask
//
//  Created by Vladimir on 20.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TTTQuoteCell: UITableViewCell {

    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var tickerLabel: UILabel!
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var exchangeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var topStack: UIStackView!
    
    var cellModel: BehaviorRelay<TTTQuote>? {
        didSet {
            if oldValue == nil {
                mascarade()
            } else {
                oldPrice = oldValue?.value.quote?.lastPrice
            }
        }
    }
    
    private var oldPrice: Double?
    
    private var model: TTTQuote? {
        return cellModel?.value
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        quoteLabel.layer.cornerRadius = 5
    }
    override func prepareForReuse() {
        cellModel = nil
        logoView.isHidden = false
        quoteLabel.textColor = TTTColors.normalText
        quoteLabel.backgroundColor = TTTColors.higlightedText
    }
    
    func mascarade() {
        setName()
        exchangeLabel.text = "\(model?.quote?.lastStockExchange ?? "") | \(model?.ticker.name ?? "")"
        setupPriceLabel()
        setupQuoteLabel()
    }
    
    private func setName() {
        if let logo = model?.logo {
            logoView.isHidden = false
            logoView.image = logo
        } else {
            logoView.isHidden = true
        }
        tickerLabel.text = model?.ticker.defaultTicker
    }
    
    private func setupPriceLabel() {
        guard let lastPrice = model?.quote?.lastPrice, let difPoints = model?.quote?.diffPoints else {
             priceLabel.text = "-"
            return
        }
        priceLabel.text = "\(lastPrice.formattedString(minFraction: 2, maxFraction: 2, withSign: false)) ( \(difPoints.formattedString(minFraction: 2, maxFraction: 6, withSign: true)) )"

    }
    
    private func setupQuoteLabel(){
        guard let quote = model?.quote?.diffPercent else {
            quoteLabel.text = "-"
            return
        }
        quoteLabel.text = "\(quote.formattedString(minFraction: 0, maxFraction: 2, withSign: true))%"
        guard let trend = model?.quote?.trend, trend != .none else {
            setQouteColor()
            return
        }
        guard oldPrice != model?.quote?.lastPrice else {
            setQouteColor()
            return
        }
        let trandColor = trend == .up ? TTTColors.positiveTrandColor : TTTColors.negativeTrandColor
        quoteLabel.textColor = TTTColors.higlightedText
        quoteLabel.backgroundColor = trandColor
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.setQouteColor()
        }
        
    }
    
    private func setQouteColor(){
        quoteLabel.backgroundColor = TTTColors.higlightedText
        guard let quote = model?.quote?.diffPercent, quote != 0 else {
            quoteLabel.textColor = TTTColors.normalText
            return
        }
        quoteLabel.textColor = quote > 0 ? TTTColors.positiveTrandColor : TTTColors.negativeTrandColor
    }
}

