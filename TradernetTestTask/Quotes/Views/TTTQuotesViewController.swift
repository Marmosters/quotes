//
//  TTTQuotesViewController.swift
//  TradernetTestTask
//
//  Created by Vladimir on 20.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TTTQuotesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = TTTQuotesViewModel()
    let bag = DisposeBag()
    
    private var spinnerView: TTTLoadingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupLoadingView()
        viewModel.getTickers()
    }

    func fetchDataDidStart() {
        spinnerView = TTTLoadingView(frame: .zero)
        spinnerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(spinnerView)
        spinnerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        spinnerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        spinnerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        spinnerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        spinnerView.startSpinner()
    }
    
    func fetchDataDidEnd() {
        spinnerView.stopSpinner()
        spinnerView = nil
    }
    
    private func setupTableView() {
        viewModel.tableModel.bind(to: tableView.rx.items(cellIdentifier: TTTNibs.qouteCellIdentifer, cellType: TTTQuoteCell.self)) { row, model, cell in
            let cellModel = model
            cellModel.asObservable()
                .observeOn(MainScheduler.instance)
                .subscribe({_ in
                cell.mascarade()
                } ).disposed(by: self.bag)
            cell.cellModel = cellModel
        }.disposed(by: bag)
    }
    
    private func setupLoadingView() {
        viewModel.loadingState.subscribe(onNext: { [weak self] value in
            value == .loadingDidStart ? self?.fetchDataDidStart() : self?.fetchDataDidEnd()
            
        }).disposed(by: bag)
    }
    
}
