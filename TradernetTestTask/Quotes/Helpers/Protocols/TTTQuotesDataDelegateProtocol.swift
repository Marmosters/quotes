//
//  TTTQuotesDataDelegateProtocol.swift
//  TradernetTestTask
//
//  Created by Vladimir on 21.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//

import Foundation

protocol TTTQuotesDataDelegate: class {
    var quotesServer: String {get}
    var tickersToWatch: [String] {get}
    
    func quoteDidUpdated(_ quotes:[TTTQuoteUpdate] )
    func quotesUpdateServiceDidConnected()
    func quotesUpdateServiceDidDisconected()
}
