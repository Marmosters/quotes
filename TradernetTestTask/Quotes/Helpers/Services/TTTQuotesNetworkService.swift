//
//  TTTQuotesNetworkService.swift
//  TradernetTestTask
//
//  Created by Vladimir on 20.09.2020.
//  Copyright © 2020 Vladimir. All rights reserved.
//



import UIKit
import SocketIO

class TTTQuotesNetworkService {
    
    var delegate: TTTQuotesDataDelegate?
    
    private lazy var manager = SocketManager(socketURL: URL(string: delegate?.quotesServer ?? "")!, config: [.log(true), .compress])
    private lazy var socket = manager.defaultSocket
    
    func subscribeToQuotes(_ tickers: [String]) {
        guard let tickers = delegate?.tickersToWatch, !tickers.isEmpty else {return}
        
        socket.on(clientEvent: .connect) {[weak self] data, ack in
            self?.socket.emit("sup_updateSecurities2", tickers)
            self?.delegate?.quotesUpdateServiceDidConnected()
        }
        
        socket.on(clientEvent: .disconnect) {[weak self] data, ack in
            self?.delegate?.quotesUpdateServiceDidDisconected()
        }
        
        socket.on("q") { data, ack in
            guard  let update = data[0] as? [String: Any], let updateData = update["q"] as? [[String: Any]] else {return}
        
            var updatedQuotes = [TTTQuoteUpdate]()
            updateData.forEach({updatedQuotes.append(TTTQuoteUpdate(withDict: $0))})
            self.delegate?.quoteDidUpdated(updatedQuotes)
        
        }

        self.socket.connect()
    }
    
    func unSubscribeFromQuotes() {
        socket.disconnect()
    }
    
    func getTickerInfo(ticker: String, completion: @escaping ((TTTQuote?, Error?) -> ())) {
        let urlString = "https://tradernet.ru/api/get-security-info-json?ticker=\(ticker)"
        guard let url = URL(string: urlString) else {
            completion(nil, TTTError.badURL)
            return
        }
        DispatchQueue.global(qos: .userInitiated).async {
            let session = URLSession.shared
            let task = session.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                    return
                }
                do {
                    let result = try JSONDecoder().decode(TTTTicker.self, from: data)
                    var quote = TTTQuote(ticker: result)
                    if let logoUrl = URL(string: "https://tradernet.ru/logos/get-logo-by-ticker?ticker=\(result.ntTicker.lowercased())"),
                        let logoData = try? Data(contentsOf: logoUrl){
                        if let image = UIImage(data: logoData), image.size.width > 1 {
                            quote.logo = image
                        }
                    }
                    completion(quote, nil)
                } catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            task.resume()
        }
    }
}
